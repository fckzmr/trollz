import random
import re
import time

import undetected_chromedriver.v2 as uc
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

DATA = {"prenoms": [], "noms": [], "domains": [], "code_postal": [], "ua": []}
for file in ["prenoms", "noms", "domains", "code_postal", "ua"]:
    with open(f"{file}.txt") as f:
        d = f.readlines()
    DATA[file] = [f.strip() for f in d]


def generate_zmr_name():
    prenom = random.choice(DATA["prenoms"])
    nom = random.choice(DATA["noms"])
    domain = random.choice(DATA["domains"])
    email = f"{f_remove_accents(prenom)}.{f_remove_accents(nom)}@{domain}"
    code_postal = random.choice(DATA["code_postal"])
    ua = random.choice(DATA["ua"])
    return prenom, nom, email, code_postal, ua


def f_remove_accents(old):
    new = old.lower()
    new = re.sub(r'[àáâãäå]', 'a', new)
    new = re.sub(r'[èéêë]', 'e', new)
    new = re.sub(r'[ìíîï]', 'i', new)
    new = re.sub(r'[òóôõö]', 'o', new)
    new = re.sub(r'[ùúûü]', 'u', new)
    return new


if __name__ == '__main__':
    counter = 0
    driver = uc.Chrome()
    while True:
        prenom, nom, email, code_postal, ua = generate_zmr_name()
        headers = {"user-agent": ua}
        response = driver.get("https://www.croiseedeschemins-ez.fr/inscription")
        if driver.title == "Venez à la rencontre d'Eric Zemmour":
            # token = driver.find_element_by_xpath("//meta[@name='csrf-token']/@content")[0]
            data = {
                # "authenticity_token": token,
                "page_id": 20,
                "return_to": "https://www.croiseedeschemins-ez.fr/inscription",
                "email_address": "",
                "signup[first_name]": prenom,
                "signup[last_name]": nom,
                "signup[email]": email,
                "signup[submitted_address]": code_postal,
                "commit": "Envoyer"
            }
            prenom_form = driver.find_element(value="(//input[@name='signup[first_name]'])[1]", by=By.XPATH)
            nom_form = driver.find_element(value="(//input[@name='signup[last_name]'])[1]", by=By.XPATH)
            email_form = driver.find_element(value="(//input[@name='signup[email]'])[1]", by=By.XPATH)
            code_postal_form = driver.find_element(value="(//input[@name='signup[submitted_address]'])[1]", by=By.XPATH)

            prenom_form.clear()
            nom_form.clear()
            email_form.clear()
            code_postal_form.clear()

            prenom_form.send_keys(prenom)
            time.sleep(1)

            nom_form.send_keys(nom)
            time.sleep(1)

            email_form.send_keys(email)
            time.sleep(1)

            code_postal_form.send_keys(code_postal)
            time.sleep(1)

            commit_btn = driver.find_element(value="//input[@name='commit']", by=By.XPATH)
            commit_btn.click()

            try:
                success = WebDriverWait(driver, 10).until(
                    expected_conditions.presence_of_element_located((By.XPATH, "(//button[@aria-label='Close'])[2]"))
                )
                success.click()
            except Exception as e:
                print(e)


            counter += 1
            print(f"FCK ZMR {counter}: {prenom} {nom} registered")
            time.sleep(5)
